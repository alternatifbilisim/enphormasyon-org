<?php
/*
 * GPL v3 Licensed
 * For alternatifBilisim.org by ideaLibre
 */
//include DB_LIB;
require_once MAIL_LIB;

class Sign {
    private $db;
    private $mail;
    private $table;
    public $signer = array();

    public function __set($name, $value)
    {
        $this->signer[$name] = $value;
    }


    function __construct() {
        $this->table="signers";
        $this->db = new DB(DB_NAME, DB_HOST, DB_USER, DB_PASS);
        $this->mail = new PHPMailer(true);
        $this->mail->Host = "mail.enphormasyon.org";
        $this->mail->Port = "587";
        $this->mail->SMTPAuth = true;
        $this->mail->Username = "noreply@enphormasyon.org";
        $this->mail->Password = "******************";
        $this->mail->CharSet = "utf-8";
    }

    public function addNewSign(){
        $qry = "SELECT id, ccode, confirmation FROM " . $this->table . " WHERE email = '" . $this->signer['email'] . "'";
        $this->db->query($qry);
        if($this->db->numRows($this->db->lastResult) > 0){
            $row = $this->db->fetchNextObject();
            if($row->confirmation == "Done"){
                return -1; //die("bu kayıt zaten var ve onaylı");
            }else{
                return -2; //die("bu kayıt zaten var ama onaylanmamış..");
            }
        }elseif($this->db->numRows($this->db->lastResult) == 0){
            $qry = "insert into " . $this->table . "(name) values ('" . $this->signer['name'] ."');";
            $this->db->execute($qry);
            $this->signer['id'] = $this->db->lastInsertedId();
            if($this->updateSign()){
                return $this->sendConfirmationEmailAndUpdateDB();
            }
        }
    }

    public function reSendConfirmationMail(){
        $qry = "SELECT id, name, surname, email FROM " . $this->table . " WHERE CONCAT(MD5(name), MD5(email)) = '" . $this->signer['hash'] . "'" ;
        if($this->db->numRows($this->db->query($qry)) == 0){
            return "false";
        }elseif($this->db->numRows($this->db->query($qry)) == 1){
            $row = $this->db->fetchNextObject();
            $this->signer['id'] = $row->id;
            $this->signer['name'] = $row->name;
            $this->signer['surname'] = $row->surname;
            $this->signer['email'] = $row->email;
            if($this->sendConfirmationEmailAndUpdateDB()) return "true";
            return "false";
        }else{ die("Bir hata oluştu..");}
    }

    public function confirmSign(){
        $qry = "SELECT id, ccode, confirmation FROM " . $this->table . " WHERE email = '" . $this->signer['RetEmail'] . "'";
        if($this->db->numRows($this->db->query($qry)) == 0){
             return -2;
        }elseif($this->db->numRows($this->db->query($qry)) == 1){
            $row = $this->db->fetchNextObject();
            if($row->confirmation == "Done"){
                return -1;
            }elseif($row->ccode == $this->signer['RetcCode']){
                $qry = "UPDATE " . $this->table . " SET confirmation = 'Done' WHERE id = '" . $row->id ."'";
                if($this->db->execute($qry)){
                    return 0;
                }else{
                    return -4;
                }
            }else{
                return -3;
            }
        }
    }

    protected function updateSign(){
        $qry = "UPDATE " . $this->table . " SET "
            ."name = '". $this->signer['name'] . "', "
            ."surname = '". $this->signer['surname'] . "', "
            ."email = '". $this->signer['email'] . "', "
            ."city = '". $this->signer['city'] . "', "
            ."profession = '". $this->signer['profession'] . "', "
            ."message = '". $this->signer['message'] . "', "
            ."unvisible_surname = '". $this->signer['unvisible_surname'] . "', "
            ."ip = '". $this->signer['ip'] . "', "
            ."browser = '". $this->signer['browser'] . "' "
            ."WHERE id = " . $this->signer['id'];

        if($this->db->execute($qry)) return True;
        return False;
    }

    private function sendConfirmationEmailAndUpdateDB(){
        $cCode = $this->sendConfirmationEmail();
        if($cCode){
            $qry = "UPDATE " . $this->table . " SET "
                ."ccode = '" . $cCode . "' WHERE id = '" . $this->signer['id'] . "'";
            if($this->db->execute($qry)){
                return True;
            }
        }else{
            return False; //die("mail göndermede hata oluştu...");
        }
    }

    private function sendConfirmationEmail()
    {
        $cCode = $this->generateConfirmationCode();
        $url = "http://www.enphormasyon.org/?a=confirmSigner&c=" . $cCode . "&email=" . $this->signer['email'];
        $mesaj = CONFIRMATION_MESSAGE . $url . "<br /><br />" . SOCIAL_LIKNS_MESSAGE;

        // Eğer epsotada hata olduysa mesaja gelen epostayı yaz ve hata bildir..
        if($this->signer['email'] == ""){$this->signer['email'] = "info@enphormasyon.org"; $mesaj .= "<br /><br />Hata: Eposta yanlış. Kullanıcı tarafından girilen eposta:" . $_POST['email'];}

        $nohtml = preg_replace('#<br\s*/?>#i', "\n", preg_replace('#<\s*/?p\s*>#i', "\n", $mesaj));
        $nohtml = preg_replace("#\<a.+href\=[\"|\'](.+)[\"|\'].*\>.*\<\/a\>#U","$1",$nohtml);

        try {
            $this->mail->AddAddress($this->signer['email'], $this->signer['name'] . " " . $this->signer['surname']);
            $this->mail->SetFrom("noreply@enphormasyon.org", "Enphormasyon");
            $this->mail->Subject = 'Enphormasyon İmza Kampanyası Onayı';
            $this->mail->AltBody = $nohtml; // optional - MsgHTML will create an alternate automatically
            $this->mail->MsgHTML($mesaj);
            $this->mail->Send();
            return $cCode;
        } catch (phpmailerException $e) {
            //echo $e->errorMessage(); //Pretty error messages from PHPMailer
            return False;
        } catch (Exception $e) {
            //echo $e->getMessage(); //Boring error messages from anything else!
            return False;
        }
    }

    private function generateConfirmationCode(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < 32; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        $qry = "SELECT id FROM " . $this->table . " WHERE ccode = '" . $randomString . "'";
        $this->db->query($qry);
        if($this->db->numRows($this->db->lastResult) == 0){
            return $randomString;
        }else{
            return $this->generateConfirmationCode();
        }
    }

    public function sendNewsletter($emails = "ALL"){
        $mesaj = "
        <p>enPHORMasyon kampanyamızın değerli destekçileri,</p>
<p>Türkiye’de kullanıcının kişisel gizliliğini ve iletişimin mahremiyeti temel hakkını çiğneyerek faaliyet gösteren Phorm firmasına karşı yürütülen İmza Kampanyamıza verdiğiniz destek ses getirdi. Phorm şirketi kampanyamızdan rahatsız oldu. Avukatları aracılığıyla da tüm basına ve derneğimize karşı ihtarlar gönderdiler. Biz Phorm’un faaliyetlerini BTK'ya şikayet ettik ve haklarında suç duyurusunda bulunduk. Ayrıca siyasi parti ayırmaksızın bazı milletvekillerine bilgi notları gönderdik ve yakında bu konu mecliste de gündeme gelecek.</p>
<p>Ancak, bu süreç zarfında kamuoyunun bilgilenmesi ve kampanyanın daha fazla insana ulaşması büyük önem arz ediyor. Bu konuda daha çok desteğinize ihtiyacımız var.</p>
<p>Şu an itibariyle imza sayımız 2500 civarında. Bu sayıyı arttırmak için çaba göstermemiz gerekiyor. Twitter ve sosyal ağlarda kampanyamızdan bahsederek yaygınlaşmasına yardımcı olabilirsiniz. Çevrenizdeki insanları imza kampanyamıza yönlendirebilirsiniz.</p>
<p>Ulusal televizyonlar ve gazeteler için büyük bir reklam veren TTNet, Türkiye’de neredeyse tekel konumunda bulunan büyük sermaye grubudur. Bundan ötürü, ulusal medyada kampanyamıza karşı adı konulmamış bir sansür uygulanıyor. Phorm şirketinin TTnet üzerinden İnternet kullanıcılarının iletişim gizliliğini ve mahremyetini ihlal eden faaliyetlerini kamuoyuna duyurmak temel bir ifade özgürlüğüdür. Ancak, Derneğimiz’in bu kamusal sorumluluk bilinci ile yürüttüğü kampanyasına sansür uygulanmakta. Şimdi de Kampanyamıza ve Websitemize yönelik bir tür sindirme ve korkutma operasyonu gerçekleştirilmeye çalışılıyor. Bu sansürü kırmanın tek yolu Phorm’a karşı yurttaş baskısı
yaratmaktır.</p>
<p>Aşağıdaki bağlantıları kullanarak kampanyamızın yaygınlaşmasına yardımcı olabilirsiniz.</p>

<p>Facebook sayfamız:<br />
http://www.facebook.com/EnPHORMasyon</p>

<p>İmza kampanyamız:<br />
http://www.enphormasyon.org/#contact</p>

<p>Twitter Hashtagleri:<br />
#Phorm #ttnetGezinti</p>
        ";

        $nohtml = preg_replace('#<br\s*/?>#i', "\n", preg_replace('#<\s*/?p\s*>#i', "\n", $mesaj));
        $nohtml = preg_replace("#\<a.+href\=[\"|\'](.+)[\"|\'].*\>.*\<\/a\>#U","$1",$nohtml);
        $p = (intval($_GET['p']) == 0) ? 1 : intval($_GET['p']);

        $per_page = 100;
        $qry = "select count(*) as toplam_imza_sayisi from signers";
        $this->db->query($qry);
        $row = $this->db->fetchNextObject();
        $toplam_imza = $row->toplam_imza_sayisi;

        $l = abs($p-1) * $per_page . "," . $per_page;
        $qry = "select concat(name, ' ', surname) as name, email, id from signers ORDER BY id ASC LIMIT " . $l;
        $pages = ceil($toplam_imza / $per_page);
        $this->db->query($qry);
        while($row = $this->db->fetchNextObject()){
            try {
                $this->mail->ClearAddresses();
                $this->mail->AddAddress($row->email, $row->name);
                $this->mail->SetFrom("noreply@enphormasyon.org", "Enphormasyon");
                $this->mail->Subject = 'Enphormasyon Kampanyası';
                $this->mail->AltBody = $nohtml; // optional - MsgHTML will create an alternate automatically
                $this->mail->MsgHTML($mesaj);
                $this->mail->Send();
                //echo $mesaj . '<hr />' . $nohtml;
                echo $row->id . '-' . $row->email  . '-' . $row->name . "......gitti<br />";

            } catch (phpmailerException $e) {
                echo $e->errorMessage(); //Pretty error messages from PHPMailer
            } catch (Exception $e) {
                echo $e->getMessage(); //Boring error messages from anything else!
            }
        }
    }

} //class ends

?>
