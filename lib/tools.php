<?php
    function unsetAllCookies(){
        if (isset($_COOKIE)) {
            foreach($_COOKIE as $cookie) {
                setcookie($cookie, '', time() - 36000);
                setcookie($cookie, '', time() - 36000, "/");
            }
        }
    }
    
    function unsetAllSessionVar(){
         foreach($_SESSION as $v => $k){
            $_SESSION[$v] = NULL;
        }
    }
    
    function pre($r){
        echo "<pre>";
        print_r($r);
        echo "</pre>";
        }    
    function print_all( $obj, $title = false ){
        print "\n<div style=\"font-family:Arial;\">\n";
        if( $title ) print "<div style=\"background-color:red; color:white; font-size:16px; font-weight:bold; margin:0; padding:10px; text-align:center;\">$title</div>\n";
        print "<pre style=\"background-color:yellow; border:2px solid red; color:black; margin:0; padding:10px;\">\n\n";
        var_export( $obj );
        print "\n\n</pre>\n</div>\n";
    }
    
    
    function showAllVars($die="False"){
    print_all($_SESSION, "session");
    print_all($_SESSION['id'], "session_id");
    print_all($_GET, "get");
    print_all($_POST, "post");
    print_all($_SERVER, "server");
    print_all($_REQUEST, "request");
    print_all($_COOKIE, "cookie");
    print_all(session_id(), "session_id");
    echo __FILE__;
    if ($die == "True")
        die();
}
    function newCaptchaImg(){
        $_SESSION['captcha'] = captcha( array(
            'code' => '',
            'min_length' => 4,
            'max_length' => 4,
            'characters' => 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',
            'min_font_size' => 12,
            'max_font_size' => 16,
            'color' => '#000',
            'angle_min' => 0,
            'angle_max' => 0,
            'shadow' => false,
            'shadow_color' => '#CCC',
            'shadow_offset_x' => 0,
            'shadow_offset_y' => 0
        ));
        return $_SESSION['captcha']['image_src'];
    }
    function checkCaptcha($code){
        return ($_SESSION['captcha']['code'] == $code) ? "true" : "false";
    }
?>
