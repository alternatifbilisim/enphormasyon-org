<?php
define('LIB', 'lib/');
define('DB_LIB', 'lib/db.php');
define('DB_NAME', 'imza_kamp');
define('DB_HOST', 'localhost');
define('DB_USER', 'imza');
define('DB_PASS', '***************');

define('FACEBOOK_PAGE', "https://www.facebook.com/EnPHORMasyon");
define('HASHTAG', "#oyunagelme");
define('TWITTER_ACCOUNT', "@altbilisim");
define('TWEET', "https://twitter.com/intent/tweet?text=Ki%C5%9Fisel%20verilerimizin%20gizlili%C4%9Fine%20sayg%C4%B1...&url=http%3A%2F%2Fwww.enphormasyon.org%2F&hashtags=Phorm,ttnetGezinti&via=altbilisim");

define('MAIL_LIB', 'lib/phpmailer/class.phpmailer.php');

$c_message = '<p>ENPHORMASYON Kampanyasına katıldığınız için teşekkürler. </p>'
.'<p>Onay için lütfen aşağıdaki bağlantıya tıklayın ya da kopyalayıp tarayıcınızın adres satırına yapıştırın. Onay bağlantısı: <br/> <br/></p>';

$slm = '<p>Sosyal medyada kampanyamızdan bahsedip diğer insanların haberdar olmasını sağlayabilirsiniz:<br />'
.'<a href="' . FACEBOOK_PAGE . '" alt="Facebook Sayfamıza Destek Ver" title="Facebook Sayfamıza Destek Ver">Facebook\'ta Paylaş</a><br/>'
.'<a href="' . TWEET . '" alt="Twitterda Paylaş" title="Twitterda Paylaş">Twitter\'ta Paylaş</a></p>'
.'<p>Monitörümüzü izleyen, gizliliğimize dikilmiş bu gözleri hep birlikte kapatalım..</p>';


define('SOCIAL_LIKNS_MESSAGE', $slm);
define('CONFIRMATION_MESSAGE', $c_message);

/*
 * SQL Statement:
 *
CREATE  TABLE IF NOT EXISTS `imza_kamp`.`signers` (
  `id` INT UNSIGNED NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NULL ,
  `surname` VARCHAR(45) NULL ,
  `email` VARCHAR(255) NULL ,
  `city` VARCHAR(45) NULL ,
  `profession` VARCHAR(45) NULL ,
  `message` TEXT NULL ,
  `unvisible_surname` TINYINT(1) NULL ,
  `ip` VARCHAR(20) NULL ,
  `browser` VARCHAR(120) NULL ,
  `ccode` VARCHAR(32) NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) ,
  UNIQUE INDEX `ccode_UNIQUE` (`ccode` ASC) )
ENGINE = MyISAM
*/

?>
