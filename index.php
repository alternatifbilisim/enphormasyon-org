<?php
    session_start();
    require_once ('config.php');
    require_once (LIB . 'php-captcha/simple-php-captcha.php');
    require_once (LIB . 'tools.php');
    include DB_LIB;
    $db = new DB(DB_NAME, DB_HOST, DB_USER, DB_PASS);

if(isset($_GET['a']) && $_GET['a']=="confirmSigner" && isset($_GET['c'])){
?>
<html class="no-js" lang="tr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Phorm TTNET - İmza Onayı</title>
    <meta name="description" content="Phorm'un Türkiye'deki faaliyetlerine karşı imza kampanyası'">
    <meta name="keywords" content="phorm, dpi, ttnet, gezinti, dijital gözetim">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/libs/modernizr-2.5.3-respond-1.1.0.min.js" type="text/javascript"></script>
</head>
<body>
    <div id="wrapper">
        <header>
            <div class="div-logo">
                <a href="?clearCache=True" alt="Home" title="Home">
                    <img src="img/logo.png" alt="Home" title="Home" />
                </a>
                <h5>Gizliliğinize dikilmiş bir çift göz</h5>
            </div>
        </header>
<?php
    $c = preg_replace('/[^a-zA-Z0-9]/s', '', $_GET['c']);
    include LIB . 'imza.php';
    $signer = new Sign;
    $signer->signer['RetEmail'] = $_GET['email'];
    $signer->signer['RetcCode'] = $_GET['c'];
    $r = $signer->confirmSign();
    if($r === -1){
        echo '<p style="float:left; margin: 100px 0 20px; width: 100%;">Aktivasyonunuz zaten yapılmış. Teşekkür ederiz..</p>';
    }elseif($r === -2){
        echo '<p style="float:left; margin: 100px 0 20px; width: 100%;">Böyle bir eposta yok. Lütfen yeniden imza atmayı deneyiniz..</p>';
    }elseif($r === -3){
        echo '<p style="float:left; margin: 100px 0 20px; width: 100%;">Yanlış onay kodu. Lütfen gönderilen bağlantıyı adres satırına hatasız olarak yapıştırdığınızdan emin olun. Ya da yeni bir kod üretmek için imza formunu yeniden doldurun.</p>';
    }elseif($r === -4){
        echo '<p style="float:left; margin: 100px 0 20px; width: 100%; color:#888;">Özür dileriz. Sistemde bir hata oldu. Hata ilgililere bildirildi. Lütfen bir süre sonra yeniden deneyiniz. Teşekkür ederiz.</p>';
    }elseif($r === 0){
        echo '<p style="float:left; margin: 100px 0 20px; width: 100%; color:#888;">İmzanız onaylandı. Teşekkür ederiz..</p>';
    }
        echo '<p style="float:left; margin: 0px 0 20px 0px; width: 100%; color:#888;">Sosyal medyada kampanyamızdan bahsedip diğer insanların haberdar olmasını sağlayabilirsiniz:</p>';
        echo '<p style="float:left; margin: 0px 0 20px 0px; width: 100%; color:#888;"><a href="' . FACEBOOK_PAGE . '" style=" color:#cc8888;" alt="Facebook Sayfamıza Destek Ver" title="Facebook Sayfamıza Destek Ver">Facebook sayfamıza yorumlarınızı bırakabilir, destek için sayfamızı beğenebilirsiniz.</a></p>';
        echo '<p style="float:left; margin: 0px 0 20px 0px; width: 100%; color:#888;"><a href="' . TWEET . '" style=" color:#cc8888;" alt="Twitterda Paylaş" title="Twitterda Paylaş">Twitter\'ta Paylaşarak kampanyayı takipçilerinize duyurabilirsiniz..</a></p>';
        echo '<p style="float:left; margin: 0px 0 20px 0px; width: 100%; color:#888;">Monitörümüzü izleyen, gizliliğimize dikilmiş bu gözleri hep birlikte kapatalım..</p>';
        echo '<p style="float:left; margin: 0px 0 20px 0px; width: 100%; color:#888;">Bu pencereyi kapatabilirsiniz.</p>';
        die();
?>
</body>
</html>
<?php
}


if(isset($_GET['p']) && $_GET['signerList'] == "T" && !preg_match('/[^0-9]/s', $_GET['p'])){

if($_GET['ns']){
    header('Content-Type: text/html; charset=utf-8');
?>
<html class="no-js" lang="tr">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</head>
<body>
<?php
    //include LIB . 'imza.php';
    //$signer = new Sign;
    //$signer->sendNewsletter();
    echo "500 internal server error";
?>
</body>
</html>
<?php
die("");
}

?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="tr">
<!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>İmza Listesi | Phorm, DPI ve TTNET</title>
    <meta name="description" content="Phorm'un Türkiye'deki faaliyetlerine karşı imza kampanyası'">
    <meta name="keywords" content="phorm, dpi, ttnet, gezinti, dijital gözetim">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/style.css">
    <link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <script src="js/libs/modernizr-2.5.3-respond-1.1.0.min.js" type="text/javascript"></script>
</head>
<body>
    <div id="wrapper">

        <header>
            <div class="div-logo">
                <a href="index.php" alt="Home" title="Home">
                    <img src="img/logo.png" alt="Home" title="Home" />
                </a>
                <h5>Gizliliğinize dikilmiş bir çift göz</h5>
            </div>
            <div class="div-mainmenu">

                <ul id="nav">
                    <li>
                        <a href="index.php#home" class="current">giriş</a></li>
                    <li>
                        <a href="index.php#about">açıklama</a></li>
                    <li class="li-drop">
                        <a href="#">detaylı bilgi</a>

                        <ul>
                            <li>
                                <a href="detay.html">Phorm, DPI ve TTNET</a>
                            </li>
                            <li>
                                <a href="phorm_aciklama.html#">Phorm Resmi Açıklama</a>
                            </li>
                            <li>
                                <a href="videolar.html">Videolar</a>
                            </li>
                            <li>
                                <a href="btk_sorular.html">BTK'ya Sorular</a>
                            </li>
                            <li>
                                <a href="baglantilar.html">Bağlantılar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="index.php#contact">imza kampanyası</a></li>
                    <li>
                        <a href="english.html">english</a></li>
                </ul>

            </div>
        </header>

        <!-- Begin Content: About  -->
        <section id="home" class="div-content">
            <!-- Your Info Here -->
            <h2 style="margin-top: 30px;">İmza Listesi</h2>
            <div class="div-content-row">
                <div class="div-profile-text">

<?php
    $p = (intval($_GET['p']) == 0) ? 1 : intval($_GET['p']);

    //$qry = "SET NAMES utf8";
    //$db->query($qry);
    $qry = "select count(*) as toplam_imza_sayisi from signers";
    $db->query($qry);
    $row = $db->fetchNextObject();
    $toplam_imza = $row->toplam_imza_sayisi;
    $n = 30;
    $onceki = ($p == 1) ? '&laquo; Önceki Sayfa' : '<a href="?signerList=T&amp;p=' . ($p - 1) . '&amp;a=q">&laquo; Önceki Sayfa</a>';
    $sonraki = ($p >=( $toplam_imza / $n)) ? 'Sonraki Sayfa &raquo;' : '<a href="?signerList=T&amp;p=' . ($p + 1) . '&amp;a=q">Sonraki Sayfa &raquo;</a>';
    echo '<p>' . $onceki . '|' . $sonraki . ' (Toplam ' . $row->toplam_imza_sayisi . ' imza...)</p>';

    $l = abs($p-1) * $n . "," . $n;
    $qry = "select concat(name, ' ', if(unvisible_surname = 1, CONCAT(SUBSTRING(surname,1,1), '.'), surname)) as name,
    profession, city, message from signers where confirmation = 'Done' ORDER BY date DESC  LIMIT " . $l;
    $db->query($qry);
    echo '<div id="imza-listesi"><table>';
    echo '<tr class="title"><td>İsim</td><td>Meslek</td><td>Şehir</td><td>Mesaj</td></tr>';
    $i = 0;
    while($row = $db->fetchNextObject()){
            $class = ((++$i % 2) == 0) ? "" : "odd";
            echo '<tr class="' . $class . '"><td>' . substr($row->name,0,30) . "</td><td>" . substr($row->profession, 0,20)
            . "</td><td>" . substr($row->city, 0,14) . "</td><td>" . substr($row->message, 0,140) . "</td></tr>";
    }
    echo '</table></div>';
?>
               </div>
            </div>
        </section>
        <!--End Content: About  -->



        <footer>
            <div class="footer-info">
                <p>
                    Theme by <a href="http://www.fredsarmento.me">Fredsarmento.me</a></p>
            </div>
            <div class="footer-menu">
                <li>
                    <a href="http://alternatifbilisim.org" class="current">alternatif bilişim</a></li>
            <div>
        </footer>

    </div>

    <script src="js/libs/jquery-1.7.2.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/libs/jquery.easing.1.3.js" type="text/javascript"></script>
    <script src="js/libs/jquery.iconmenu.js" type="text/javascript"></script>
    <script src="js/libs/jquery.quicksand.js" type="text/javascript"></script>
    <script src="js/libs/jquery.prettyPhoto.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () { $('#ul-services').iconmenu(); });
    </script>

</body>
</html>


<?php
}
if(strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){ // Sadece ajax sorguları

if(isset($_GET['a']) && $_GET['a']=="generateNewCapctha"){
    echo newCaptchaImg();
    die();
}

if(isset($_GET['a']) && $_GET['a']=="checkCapctha" && isset($_GET['c'])){
    $c = preg_replace('/[^a-zA-Z0-9_ -]/s', '', $_GET['c']);
    header('Content-Type: text/plain');
    echo trim(checkCaptcha($c));
    die();
}

if(($_GET['sendConfirmationCode'] == "T" && $_GET['a'] == session_id())){
    include LIB . 'imza.php';
    $signer = new Sign;
    $signer->signer['hash'] = $_GET['h'];
    header('Content-Type: text/plain');
    echo  $signer->reSendConfirmationMail();
}

if(($_GET['sign'] == "T" && $_GET['a'] == session_id())){
    $successResponse = '';
    $successResponse .= '<p>Desteğiniz için teşekkürler. İmzanızı onaylamak için lütfen eposta adresinize gönderilen onay bağlantısına tıklayın.</p>';
    $successResponse .= '<p>Sol taraftaki sosyal medya bağlantılarını kullanarak imza kampanyasının duyurulmasına yardımcı olabilirsiniz. Teşekkürler.</p>';
    $failResponse = '';
    $failResponse .= '<p>Bir hata oluştu. Hata sistem yöneticine iletildi. Sistem çok meşgul olabilir. Bir süre sonra yeniden denemenizi rica ederiz. Desteğiniz için teşekkürler.</p>';
    $failResponse .= '<p>Şimdi yeniden denemek için <a href="?clearCache=True">buraya tıklayın.</a></p>';
    $successButStillSignedApproved = '';
    $successButStillSignedApproved .= '<p>Verdiğiniz eposta adresi ile daha önce imza atılmış ve onaylanmış.</p>';
    $successButStillSignedApproved .= '<p>Sol taraftaki sosyal medya bağlantılarını kullanarak imza kampanyasının duyurulmasına destek olabilirsiniz. Teşekkürler.</p>';
    $successButStillSignedNotApproved = '';
    $successButStillSignedNotApproved .= '<p>Verdiğiniz eposta adresi ile daha önce imza atılmış. Fakat eposta ile onay gerçekleşmemiş. Lütfen eposta kutunuzu kotrol edin. Spam filtrenize takılmış olabilir.</p>';
    $successButStillSignedNotApproved .= '<p>Yeni bir onay kodu isterseniz lütfen <a href="#" onclick="sendConfirmationCode(\'' . md5($_POST['name']) . md5($_POST['email']) .'\');">buraya tıklayın</a>.</p>';
    $successButStillSignedNotApproved .= '<p>Başka bir eposta adresi ile denemek için <a href="?clearCache=True">buraya tıklayın.</a></p>';
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

    if($_SESSION['captcha']['code'] == $_POST['contact_captcha']){
        include LIB . 'imza.php';
        $signer = new Sign;
        $signer->name = preg_replace('/[^a-zA-Z ŞşÇçÖöĞğÜüİı]/s', ' ', $_POST['name']);
        $signer->surname = preg_replace('/[^a-zA-Z ŞşÇçÖöĞğÜüİı]/s', ' ', $_POST['surname']);

        if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
            $signer->email = $_POST['email'];
        }else{
            $signer->email = "";
        }
        $signer->city = preg_replace('/[^a-zA-Z ŞşÇçÖöĞğÜüİı]/s', ' ', $_POST['city']);
        $signer->profession = preg_replace('/[^a-zA-Z ŞşÇçÖöĞğÜüİı]/s', ' ', $_POST['profession']);
        $signer->message = preg_replace('/[^!0-9A-Za-z  \$€\?%& ŞşÇçÖöĞğÜüİı,\.\n\t\+-]/s', ' ', $_POST['message']);
        if(isset($_POST['unvisible_surname'])) $signer->unvisible_surname = 1;
        $signer->ip = preg_replace('/[^0-9]/s', '-', $_SERVER['REMOTE_ADDR']);
        $signer->browser =  preg_replace('/[^a-zA-Z0-9_ -]/s', ' ', $_SERVER['HTTP_USER_AGENT']);
        $r = $signer->addNewSign();
        if($r === -1){
            echo $successButStillSignedApproved;
        }elseif($r === -2){
            echo $successButStillSignedNotApproved;
        }elseif($r === True){
            echo $successResponse;
        }else{
            die($failResponse);
        }

    }else{
        echo $failResponse;
        //header("Location:index.php");
    }
}
}


if(!isset($_GET['a'])){
newCaptchaImg();
?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="tr">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Phorm TTNET</title>
    <meta name="description" content="Phorm'un Türkiye'deki faaliyetlerine karşı imza kampanyası'">
    <meta name="keywords" content="phorm, dpi, ttnet, gezinti, dijital gözetim">
    <meta name="author" content="pr" >
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/style.css">
    <link href="css/prettyPhoto.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <script src="js/libs/modernizr-2.5.3-respond-1.1.0.min.js" type="text/javascript"></script>
</head>
<body>


    <div id="wrapper">

        <header>
            <div class="div-logo">
                <a href="index.php" alt="Home" title="Home">
                    <img src="img/logo.png" alt="Home" title="Home" />
                </a>
                <h5>Gizliliğinize dikilmiş bir çift göz</h5>
            </div>
            <div class="div-mainmenu">

                <ul id="nav">
                    <li>
                        <a href="#home" class="current">giriş</a></li>
                    <li>
                        <a href="#about">açıklama</a></li>
                    <li class="li-drop">
                        <a href="#">detaylı bilgi</a>

                        <ul>
                            <li>
                                <a href="detay.html">Phorm, DPI ve TTNET</a>
                            </li>
                            <li>
                                <a href="phorm_aciklama.html#">Phorm Resmi Açıklama</a>
                            </li>
                            <li>
                                <a href="videolar.html">Videolar</a>
                            </li>
                            <li>
                                <a href="btk_sorular.html">BTK'ya Sorular</a>
                            </li>
                            <li>
                                <a href="baglantilar.html">Bağlantılar</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="index.php#contact">imza kampanyası</a></li>
                    <li>
                        <a href="english.html">english</a></li>
                </ul>

            </div>
        </header>

        <!--Begin Content: Home -->
        <section id="home" class="div-content">
            <div style="text-align:center;float: left; margin: 70px 0 0; width: 100%;"><p>DUYURU: <a href="btk_karar_degerlendirme.html" style="color:#c00;">BTK, TTNET ve PHORM konusunda karar verdi..</a></p></div>
            <div class="welcome-text">
                <p class="row-1">
                    dikkat!
                </p>
                <p class="row-2">
                    Phorm <span>Türkiye'de</span>
                </p>
                <p class="row-5">
                    kişisel verileriniz ve iletişiminizin gizliliğine saygı
                </p>
                <p class="row-6">
                    <a href="#about" alt="enphormasyon.org" title="ttnete tepki"><span>talep edin!</span></a>
                </p>
            </div>
            <p class="p-arrow">
                <a href="#about" class="a-arrow">açıklama<br />
                    <img src="img/arrow-down.png" /></a>
            </p>
        </section>
        <!--End Content: Home  -->

        <!-- Begin Content: About  -->
        <section id="about" class="div-content">
            <!-- Your Info Here -->
            <h2>İnternet kullanıcıları ve tüm yurttaşların dikkatine!..</h2>
            <div class="div-content-row">
                <div class="div-profile">
                    <a href="img/tehlikeye-karsi-onlem-alin.png" target="_blank">
                        <img src="img/tehlikeye-karsi-onlem-alin.png" class="effect2" /></a>
                    <div class="div-sublinks">
                        <ul>
                            <li>
                                <a href="<?php echo FACEBOOK_PAGE ?>" class="btn fb" alt="Facebook Sayfamıza Destek Ver" title="Facebook Sayfamıza Destek Ver"></a>
                            </li>
                            <li>
                                <a href="<?php echo TWEET ?>" class="btn twt" alt="Twitter'da Paylaş'" title="Twitter'da Paylaş"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="div-profile-text">
                    <p>
                        Kişisel verilerin ve iletişimin gizliliğini ihlal ettiği, temel insan hak ve özgürlüklerine aykırı davrandığı gerekçesiyle, yurttaşların tepki gösterdiği, Avrupa Parlementosu'nun Türkiye'yi de içeren bir kararıyla<sup title="detaylı bilgiler sayfalarına bakınız!..">1</sup>  da AB sınırlarında faaliyetleri yasadışı hale gelen Phorm, Türkiye'de faaliyetlerine başladığını duyurdu.<sup title="detaylı bilgiler sayfalarına bakınız!..">2</sup> Şirket, Türkiye'nin fiili tekel durumundaki servis sağlayıcısı TTNET ile anlaştı. Aynı tarihlerde TTNET, GEZİNTİ adlı servisini başlattı. Bütün dünyada büyük tepkiler gören ve yasal yaptırımlarla faaliyetleri engellenen Phorm konusunda Türkiye'de İnternet kullancılarını kişisel verilerinin gizliliğine sahip çıkmaya ve tepki göstermeye çağırıyoruz.
                    </p>
                    <br />
                    <p>
                        TTNET'in yeni hizmeti GEZİNTİ adlı servisin "gezinti.com" adresindeki sitesinde Phorm ile ilgili herhangi bir ifade yok. Fakat hizmetin Phorm teknolojileri ile sunulduğunu tahmin etmek çok da zor değil.
                    </p>
                    <h4>Phorm nedir, ne yapar?</h4>
                    <p>
                        Phorm kendi ifadesiyle kullanıcıların internet deneyimlerini daha fazla kişiselleştirmeyi amaçlıyor. Bu tam olarak şu demek: Sizi gezdiğiniz siteler, tıkladığınız reklamlar, izlediğiniz videolar, doldurduğunuz formlar vb. aracılığı ile profilliyor. Elde edilen profile göre ticari olan/olmayan içerikler sunuyor. Yani İnternette neler yaptığınızı/davranışlarınızı izleyip ona uygun reklamlar ve içerikler sunuyor.
                    </p>
                    <h4>Bunun ne zararı var?</h4>
                    <p>
                        Öncelikle izleniyorsunuz. Fakat bu izleme DPI<sup title="detaylı bilgiler sayfalarına bakınız!..">3</sup> olarak bilinen teknolojilerle yapılıyor. Bu teknoloji sadece sizin bilgisayarınız ya da tarayıcınızla ilgilenmiyor. İnternet hattınızı izleyip, gelen giden tüm verilerinizi analiz ediyor. Bu durumun siz İnternette dolaşırken başınızda durup monitörünüzü izleyen bir çift gözden farkı yok.
                    </p>
                    <br />
                    <p>
                        İkinci büyük problem İnternet'in geleceğini tehdit etmesidir. İnternet gayri-merkezi, kendi dinamikleri ile gelişen, tarafsız küresel bir ağdır. Ağ teknolojileri "net tarafsızlığı" ilkesine göre çalışır. İnternetteki trafiğiniz, üzerinde gönderici/alıcı bilgileri bulunan paketler halinde akar. Paketler gönderir, paketler alırsınız. Ağ üzerindeki herhangi bir düğüm (paketinizi ulaşacağı hedefe doğru yönlendiren noktalar) sadece kaynak ve hedef adresleri okur, paketi ilgili yere teslim eder. <strong>Paketin içeriği, yani sizin erişmek veya iletmek istediğiniz bilgilerle ilgilenmez.</strong> Tüm paketlere eşit davranılır. Kimsenin paketi diğerlerinden değerli değildir. Tıpkı tarafsız çalışan bir postane gibi. DPI teknolojileri ve Phorm gibi DPI kullanarak profilleme/analiz yapan firmalar bu tarafsızlığı bozmakta, içeriklere göre paketleri tasnif etmekte, kişisel verilerimizi bilemediğimiz/kontrol edemediğimiz süreçlerden geçirmektedir. Bu da net tarafsızlığını bozmakta ve ağın yapısını <strong>'ticari önceliklere'</strong> göre şekillendirme tehlikesiyle karşı karşıya getirmektedir.
                    </p>
                    <br />
                    <p>
                        Toplanan profillerin, elde edilen bilgilerin başka ne amaçla kullanıldığını bilememekte, denetleyememekteyiz. Bu hizmet ve altyapılar şeffaf, hesap verebilir değildir.
                    </p>
                    <br />
                    <p>
                        Kişisel verilerimizin ve iletişimimizin gizliliğini ihlal eden, İnternetin dağıtık, demokratik, tarafız yapısını bozan, merkezi, karanlık teknolojileri İnternet'te istemiyoruz. TTNET ve Phorm, Türkiye'deki faaliyetleri hakkında bilgi vermeli, yapılan bu işbirliğine derhal son vermelidir.
                    </p>
                    <br />
                    <p>
                        <strong>Yurttaşları gezinti.com ve Phorm konusunda tepki göstermeye, İnternete sahip çıkmaya çağırıyoruz.</strong>
                    </p>
                    <br />
                    <p id="foot-notes" style="font-size:85%;">
                        --<br />
                        <strong>1: </strong>REGULATION (EU) No 1232/2011 OF THE EUROPEAN PARLIAMENT AND OF THE COUNCIL of 16 November 2011, L 326/37, Part3 1.1.d<br />
                        <strong>2: </strong>http://www.phorm.com/sites/default/files/2012.07.09%20TTNET%20Commercial%20Activities.pdf<br />
                        <strong>3: </strong>Detaylar sayfasına bakınız. <a href="http://www.enphormasyon.org/detay.html">http://www.enphormasyon.org/detay.html</a><br />
                    </p>
               </div>
            </div>
            <p class="p-arrow" style="padding-top:20px;">
                <a href="#services" class="a-arrow">ne yapabilirim?<br />
                    <img src="img/arrow-down.png" /></a>
            </p>
        </section>
        <!--End Content: About  -->

        <!--Begin Content: Services  -->
        <section id="services" class="div-content">
            <h2>Ne yapabilirim?</h2>
            <p class="page-desc">
                Tüm dünyada İnternet kullanıcıları Phorm ve faaliyetlerine büyük tepkiler gösterdiler. Avrupa Parlamentosu bu şirketin faaliyetlerini AB sınırları içerisinde yasakladı. Biz de Phorm'un Türkiye'deki faaliyetlerini durdurmak ve dijital gözetim olgusuna dikkat çekmek için bu kampanyayı yapıyoruz. Kampanyanın yaygınlaşması ve en geniş kesimlere ulaşması için yardımlarını bekliyoruz. </p>
            <br />
            <h3>1- Gezinti.com'dan ayrıldığına emin ol</h3>
            <div style="clear: both; width;100%; margin-top:20px; float:left;">
            <p style="width:10%; float:left;">
            <a href="#image-gezinti-1"><img src="img/gezinti-hesabim.png" style="width:60px; height:40px;" /> </a><br />
            Resim -1 <br />
            <br />
            <a href="#image-gezinti-2"><img src="img/gezinti-hesabim-kapali.png" style="width:60px; height:40px;"/> </a><br />
            Resim -2 <br />
            <br />
            <a href="#image-gezinti-3"><img src="img/gezinti-hesabim-acik.png" style="width:60px; height:40px;" /> </a><br />
            Resim -3 <br />
            <br />
            <a href="#image-gezinti-4"><img src="img/gezinti-hesabim-kapat-onay.png" style="width:60px; height:40px;" /> </a><br />
            Resim -4
            </p>
            <div class="lb-overlay" id="image-gezinti-1">
                <a href="#services" class="lb-close">x</a>
                <div class="lb-overlay-wrapper">
                    <img src="img/gezinti-hesabim.png" alt="Resim -1" />
                </div>
            </div>
            <div class="lb-overlay" id="image-gezinti-2">
                <a href="#services" class="lb-close">x</a>
                <div class="lb-overlay-wrapper">
                    <img src="img/gezinti-hesabim-kapali.png" alt="Resim -2" />
                </div>
            </div>
            <div class="lb-overlay" id="image-gezinti-3">
                <a href="#services" class="lb-close">x</a>
                <div class="lb-overlay-wrapper">
                    <img src="img/gezinti-hesabim-acik.png" alt="Resim -3" />
                </div>
            </div>
            <div class="lb-overlay" id="image-gezinti-4">
                <a href="#services" class="lb-close">x</a>
                <div class="lb-overlay-wrapper">
                    <img src="img/gezinti-hesabim-kapat-onay.png" alt="Resim -4" />
                </div>
            </div>

            <p style="width:90%; float:right;">
                </a>Bunun için gezinti.com'a gidip ilk resimde görüldüğü gibi sayfanın sağ üst köşesindeki "Hesabım" bağlantısına tıklayın. <br />
                <br />
                Eğer hesabınız kapalıysa, ikinci resimdeki gibi anahtar butonun üzerinde <strong>KAPALI</strong> yazısını görmeniz gerekir. <span>Lütfen bu butona basmayın. Bu butona bastığınızda hesabınız aktifleşiyor.</span><br />
                <br />
                Eğer hesabınız açıksa, aynı yerde <strong>"Bu tarayıcı için gezinti AÇIK"</strong> ve <strong>"İnternet hattı için Gezinti AÇIK"</strong> ifadelerini göreceksiniz. Hizmeti durdurmak için <strong>"İnternet hattı için Gezinti AÇIK</strong>" yazan anahtar butonlarına tıklayın. 4. resimde görülen onay mesajı ile karşılaşacaksınız. Eğer sadece <strong>"Bu tarayıcı için gezinti AÇIK"</strong>strong> seçeneğini kapatırsanız, İnternet hattınıza bağlı diğer bilgisayarlar ve bilgisayarınızdaki mevcut diğer İnternet tarayıcıları için kapanmış <u>olmayacaktır</u>.<br />
                <br />
                Ardından yeniden hesabım sayfasına gelip KAPALI butonunu gördüğünüzden emin olun. Hatta bilgisyarınızdaki başka bir tarayıcı ile sayfaya giderek kontol yapmanız daha emin bir yol olabilir. <br />
                <br />
                Lütfen sayfadaki uyarıları dikkatle okuyun. Sayfada çıkan bazı uyarılara "HAYIR" yanıtını vermek aslında hizmetin devam etmesini istediğiniz anlamına gelebiliyor. Eğer hala emin değilseniz hemen TTNET müşteri hattını 444 0 375'den arayıp çıkarıldığınızdan emin olun.
            </p>
            </div>
            <p>&nbsp;</p>
            <h3 style="margin-top: 20px; postion:relative;">2- İmza Kampanyasına Katıl</h3>
            <p>
                Bir sonraki adımda imza kampanyamıza katılabilir ve kampanyayı facebook ve twitter hesabından duyurabilirsin.
            </p>

            <p class="p-arrow">
                <a href="#contact" class="a-arrow">kampayaya katıl<br />
                    <img src="img/arrow-down.png" /></a>
            </p>
        </section>
        <!--End Content: Services  -->


        <!--Begin Content: Contact  -->
        <section id="contact" class="div-content">
            <h2>İmza Kampanyası</h2>
            <!-- Some text here -->
            <p class="page-desc">
                Phorm'un Türkiye'deki faaliyetlerinin derhal durdurulmasını, TTNET ile işbirliğinin iptal edilmesini, Türkiye Cumhuriyeti yurttaşlarından topladıkları tüm bilgilerin imha edilmesini, düzenleyici kurum Bilgi Teknolojileri Kurumu(BTK) ve diğer ilgili resmi kurumların bu talepler doğrultusunda harekete geçmesini talep ederim.</p>

            <div class="contact-info">
                <h4>İletişim</h4>
                <p>
                    Alternatif Bilişim Derneği, <?php echo TWITTER_ACCOUNT ?>
                </p>
                <br />
                <p>
                    <strong>Eposta: </strong>info@enphormasyon.org</p>
                <p>
                    <strong>Web: </strong>
                    <a target="_self" href="http://www.alternatifbilisim.org">alternatifbilisim.org</a></p>
                <br />

                <h4>Paylaş</h4>

                <ul class="contact-social">
                    <li>
                        <a href="<?php echo FACEBOOK_PAGE ?>" class="btn fb" alt="Facebook Sayfamıza Destek Ver" title="Facebook Sayfamıza Destek Ver"></a>
                    </li>
                    <li>
                        <a href="<?php echo TWEET ?>" class="btn twt" alt="Twitter'da Paylaş'" title="Twitter'da Paylaş"></a>
                    </li>
                </ul>
                <p>&nbsp;</p>
                <br />
                <div id="imza-listesi">
                <h4 style="clear:both;">İmza Listesi</h4>
                <p>İmza atan son 10 kişi:</p>
                <br />
                <?php
                $qry = "select concat(name, ' ', if(unvisible_surname = 1, CONCAT(SUBSTRING(surname,1,1), '.'), surname)) as name,
                profession, city, message from signers where confirmation = 'Done' ORDER BY date DESC LIMIT 0,10";
                $db->query($qry);
                echo '<table>';
                echo '<tr class="title"><td>İsim</td><td>Meslek</td><td>Şehir</td></tr>';
                $i = 0;
                while($row = $db->fetchNextObject()){
                        $class = ((++$i % 2) == 0) ? "" : "odd";
                        echo '<tr class="' . $class . '"><td>' . substr($row->name,0,30) . "</td><td>" . substr($row->profession, 0,20)
                        . "</td><td>" . substr($row->city, 0,14) . "</td></tr>";
                }
                echo '</table>';
                ?>
                <p><a href="?signerList=T&p=0&a=q">Tüm imzaları listelemek için tıklayın...</a></p>
                </div>

            </div>
            <div class="contact-form" id="contact-form-container">
                <h4>İmza Kampanyasına Katıl</h4>
                <p>Verdiğiniz bilgiler hiçbir şekilde 3. kişilerle paylaşılmayacak, bu kampanya haricinde herhangi bir duyuru gönderilmeyecektir. Eposta adresiniz internet sitesinde yayınlanmayacak.</p>
                <form class="contact-form" id="sign-form">

                <ul>
                    <li>
                        <label for="contact-name">
                            <strong>İsim</strong>*</label>
                        <input type="text" name="name" value="" id="contact-name" required="">
                    </li>
                    <li>
                        <label for="contact-surname">
                            <strong>Soyisim</strong>*</label>
                        <input type="text" name="surname" value="" id="contact-surname" required="">
                    </li>
                    <li>
                        <label for="contact-unvisible-surname" style="float:rigth;">
                            <strong>Sadece baş harfi göster. Örn: Fatma Ş.</strong></label>
                        <input name="unvisible_surname" id="contact-unvisible-surname" type="checkbox">
                    </li>
                    <li>
                        <label for="contact-email">
                            <strong>E-posta</strong>*</label>
                        <input type="email" name="email" value="" id="contact-email" required="">
                    </li>
                    <li>
                        <label for="contact-city">
                            <strong>Şehir</strong>*</label>
                        <input type="text" name="city" value="" id="contact-city" required="">
                    </li>
                    <li>
                        <label for="contact-profession">
                            <strong>Meslek/Kurum</strong>*</label>
                        <input type="text" name="profession" value="" id="contact-profession" required="">
                    </li>
                    <li>
                        <label for="contact-message">
                            <strong>Mesajınız</strong>(Kalan: <span id ="counter"></span> <span id="synt_warn" style="display:"></span>)</label>
                        <textarea name="message" cols="88" rows="2"  id="contact-message"></textarea>
                    </li>
                   <li>
                        <label for="contact-unvisible-surname">
                            <strong>Güvenlik kodu:</strong><img id="contact-captcha-image" src="<?php echo $_SESSION['captcha']['image_src'] ?>" alt="Güvenlik Kodu"/>
                            <a href="javascript:void(0);" onclick="newCaptcha();return false;">Yenile</a></label>
                        <input name="contact_captcha" id="contact-captcha" type="text">
                    </li>



                    <input type="button" value="İmzala" id="contact-submit" onclick="validateAndSendForm();">
                </ul>

                </form>
            </div>
        </section>

        <!--End Content: Contact -->

        <footer>
            <div class="footer-info">
                <p>
                    Theme by <a href="http://www.fredsarmento.me">Fredsarmento.me</a></p>
            </div>
            <div class="footer-menu">
                <li>
                    <a href="http://alternatifbilisim.org" class="current">alternatif bilişim</a></li>
            <div>
        </footer>

    </div>

    <script src="js/libs/jquery-1.7.2.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/libs/jquery.easing.1.3.js" type="text/javascript"></script>
    <script src="js/libs/jquery.iconmenu.js" type="text/javascript"></script>
    <script src="js/libs/jquery.quicksand.js" type="text/javascript"></script>
    <script src="js/libs/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="js/diff_match_patch.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(function () { $('#ul-services').iconmenu(); });
        function newCaptcha(){
            $.ajax({
                    url: "?a=generateNewCapctha",
                    async: false,
                    success: function(data) {
                        $("#contact-captcha-image").attr("src", data);
                    }
            })
        }

        function sendConfirmationCode(h){
            $.ajax({
                    url: "?sendConfirmationCode=T&h=" + h + "&a=<?php echo session_id(); ?>",
                    async: false,
                    success: function(data) {
                        var text = data.replace(/(\r\n|\n|\r)/gm,"");

                        if(text == "true"){
                            var message = "Onay kodu gönderildi. Lütfen eposta kutunuzu kontrol ediniz. \n\n  Tamam basınca anasayafa yönlendirileceksiniz.";
                        }else{
                            var message = "Eposta adresinizle forma girdiğiniz isim alanı uyuşmuyor. Lütfen verdiğiniz bilgileri kontrol edip formu yeniden doldurun.\n\n Tamama basınca anasayafa yönlendirileceksiniz.";
                        }
                        alert(message);
                        window.location = 'http://www.enphormasyon.org/?clearCache=True';
                    }
            })
        }

        function checkCapctha(e){
            var captcha = $("#contact-captcha");
            $.ajax({
                    url: '?a=checkCapctha&c='+e,
                    async: false,
                    success: function(data) {
                        var text = data.replace(/(\r\n|\n|\r)/gm,"");
                        if(text == "true"){
                            var pd = $("#sign-form").serialize();
                            var url = "?sign=T&a=<?php echo session_id(); ?>";
                            $.post( url, pd,
                                  function( d ) {
                                      var content = d;
                                      $( "#contact-form-container" ).empty().append( content );
                                      $( "#contact-form-container" ).css({'display': '', 'padding':'20px', 'border': '1px dotted #c00', 'font-weight': 'bold'});
                                  }
                            );
                            warnClass(captcha, 0);
                        }else{warnClass(captcha, 1);}
                    }
            })
        }
        function validateAndSendForm(){
            //contact-name, contact-surname, contact-email, contact-city, contact-profession, contact-message, contact-unvisible-surname
            var contactName = $("#contact-name");
            var contactSurname = $("#contact-surname");
            var contactEmail = $("#contact-email");
            var contactCity = $("#contact-city");
            var contactProfession = $("#contact-profession");
            var contactMessage = $("#contact-message");
            var contactUnvisibleSurname = $("#contact-unvisible-surname");
            var emailPattern="[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
            var namePattern = "[a-zA-Z ŞşÇçÖöĞğÜüİı]+";
            var messagePattern = "[!0-9A-Za-z  \$€\?%& ŞşÇçÖöĞğÜüİı,\.\n\t\+-]+";

            var captcha = $("#contact-captcha")
            if(contactName.val() != "" && String(contactName.val()) == contactName.val().match(namePattern)[0]){
                warnClass(contactName, 0);
                if(contactSurname.val() != "" && String(contactSurname.val()) == contactSurname.val().match(namePattern)[0]){
                    warnClass(contactSurname, 0);
                    if(contactEmail.val() != "" && String(contactEmail.val()) == contactEmail.val().match(emailPattern)[0]){
                        warnClass(contactEmail, 0);
                        if(contactCity.val() != "" && String(contactCity.val()) == contactCity.val().match(namePattern)[0]){
                            warnClass(contactCity, 0);
                            if(contactProfession.val() != "" && String(contactProfession.val()) == contactProfession.val().match(namePattern)[0]){
                                warnClass(contactProfession, 0);
                                if(contactMessage.val() == ""){
                                    warnClass(contactMessage, 0);
                                    checkCapctha(captcha.val());
                                }else{
                                    if(String(contactMessage.val()) != contactMessage.val().match(messagePattern)[0]){
                                        var pt = "[^!0-9A-Za-z  \$€\?%& ŞşÇçÖöĞğÜüİı,\.\n\t\+-]";rr = new RegExp(pt, "gim");
                                        contactMessage.val(contactMessage.val().replace(rr, "-"));
                                    }
                                    if(contactMessage.val().length > 140 ){
                                        alert("Mesajınız 140 karakteri aşmamalıdır. \n\n Diğer konularda bizimle iletişim için eposta gönderebilirsiniz: info@enphormasyon.org\n\n Teşekkürler..");
                                        warnClass(contactMessage, 1);
                                    }else{
                                        checkCapctha(captcha.val());
                                        warnClass(contactMessage, 0);
                                    }
                                }
                            }else{warnClass(contactProfession, 1);}
                        }else{warnClass(contactCity, 1);}
                    }else{warnClass(contactEmail, 1);}
                }else{warnClass(contactSurname, 1);}
            }else{warnClass(contactName, 1);}
        }




        function warnClass(e, w){
            if(w == 1){
                e.css("border-color", "#c00"); e.parent().css("background-color", "#dedede");
            }else{
                e.css("border-color", "#dedede"); e.parent().css("background-color", "");
            }
        }
        $(document).ready(function()
        {
            var max_length = 140;
            $("#counter").html(max_length);
            whenkeydown(max_length);
        });

        whenkeydown = function(max_length)
        {
            $("#contact-message").unbind().keyup(function()
            {
                //check if the appropriate text area is being typed into
                if(document.activeElement.id === "contact-message")
                {
                    //get the data in the field
                    var text = $(this).val();

                    //set number of characters
                    var numofchars = text.length;

                    //set the chars left
                    var chars_left = max_length - numofchars;

                    //check if we are still within our maximum number of characters or not
                    if(numofchars <= max_length)
                    {
                        //set the length of the text into the counter span
                        $("#counter").html("").html(chars_left).css("color", "#000000");
                    }
                    else
                    {
                        //style numbers in red
                        $("#counter").html("").html(chars_left).css("color", "#FF0000");
                    }
                }
            });
        }
    </script>

</body>
</html>
<?php
}
?>
